package com.example.osmvsgmapsexample.presentation.mapbox

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.osmvsgmapsexample.domain.repository.GoogleDirectionsApiRepository
import com.example.osmvsgmapsexample.domain.repository.RoutesApiRepository
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleViewModel
import com.example.osmvsgmapsexample.presentation.main.toGeocodingResult
import com.example.osmvsgmapsexample.presentation.main.toPointMapbox
import com.example.osmvsgmapsexample.util.toGoogleApiRequestLocation
import com.example.osmvsgmapsexample.util.toGoogleApiRequestMovementType
import com.example.osmvsgmapsexample.util.toMapLocation
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import retrofit2.Call
import retrofit2.Response


class MapboxExampleViewModel : BaseMapExampleViewModel() {

    val googleDirectionsRepository: RoutesApiRepository =
        GoogleDirectionsApiRepository() // GoogleDirectionsApiRepository()

    val bottomButtonStatus = ObservableField<Pair<Int, () -> Unit>>()

    var googleApiKey: String? = null
        set(value) {
            field = value
            (googleDirectionsRepository as? GoogleDirectionsApiRepository)?.apiKey = value
        }

    var mapboxAccessToken: String? = null
        get() = field
            ?: throw IllegalArgumentException("Mapbox access token hasn't been initialized in ViewModel")

    override fun requestRoute(
        startLocation: BaseMapExampleFragment.MapLocation,
        vararg pointsOfRoute: BaseMapExampleFragment.MapLocation,
        movementType: BaseMapExampleFragment.MapMovementType
    ) {
        navigationRoute.addSource(
            googleDirectionsRepository.requestRouteFromApi(
                movementType = movementType.toGoogleApiRequestMovementType(),
                startLocation.toGoogleApiRequestLocation(),
                *pointsOfRoute.map { it.toGoogleApiRequestLocation() }.toTypedArray()
            )
        ) {
            navigationRoute.postValue(it.route?.map { it.toMapLocation() })
            navigationErrorOccured.postValue(it.areErrorOccured)
        }
    }

    override fun requestDirectGeocodingRequestSearch(locationName: String) {
        MapboxGeocoding.builder()
            .accessToken(mapboxAccessToken!!)
            .query(locationName)
            .limit(MAX_GEOCODING_RESULTS)
            .build()
            .enqueueCall(directGeocodingResponseCallback)
    }

    override fun requestReverseGeocodingRequestSearch(location: BaseMapExampleFragment.MapLocation) {
        MapboxGeocoding.builder()
            .accessToken(mapboxAccessToken!!)
            .query(location.toPointMapbox())
            .build()
            .enqueueCall(reverseGeocodingResponseCallback)
    }

    val directGeocodingResponseCallback = object : retrofit2.Callback<GeocodingResponse> {
        override fun onResponse(
            call: Call<GeocodingResponse>,
            response: Response<GeocodingResponse>
        ) {
            val resp = response.body()?.features()
            val results = resp?.map { it.toGeocodingResult() }
            directGeocodingResult.postValue(results)
        }

        override fun onFailure(call: Call<GeocodingResponse>, t: Throwable) {
            directGeocodingResult.postValue(listOf())
        }
    }

    val reverseGeocodingResponseCallback = object : retrofit2.Callback<GeocodingResponse> {
        override fun onResponse(
            call: Call<GeocodingResponse>,
            response: Response<GeocodingResponse>
        ) {
            val result = response.body()?.features()
                ?.firstOrNull()
                ?.toGeocodingResult()
            reverseGeocodingResult.postValue(result)
        }

        override fun onFailure(call: Call<GeocodingResponse>, t: Throwable) {
            reverseGeocodingResult.postValue(null)
        }
    }

    companion object {
        const val MAX_GEOCODING_RESULTS = 3
    }
}