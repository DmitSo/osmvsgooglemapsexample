package com.example.osmvsgmapsexample.presentation.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.databinding.FragmentStartBinding

class StartFragment : Fragment() {

    private lateinit var binding: FragmentStartBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.mapsExamplesRecycler.adapter = MapsExamplesListAdapter().apply {
            items = listOf(
                MapsExampleItem(R.string.example_item_gmaps, onChoosedAction = { onGoogleMapExampleChosen() }),
                MapsExampleItem(R.string.example_item_osm_whirlyglobe, onChoosedAction = { onOsmMapExampleChosen() }),
                MapsExampleItem(R.string.example_item_osm_mapbox, onChoosedAction = { onMapboxExampleChosen() })
            )
        }
    }

    private fun onGoogleMapExampleChosen() {
        findNavController().navigate(R.id.action_startFragment_to_googleMapsFragment)
    }

    private fun onOsmMapExampleChosen() {
        findNavController().navigate(R.id.action_startFragment_to_osmLibsExampleFragment)
    }

    private fun onMapboxExampleChosen() {
        findNavController().navigate(R.id.action_startFragment_to_mapboxExampleFragment)
    }
}