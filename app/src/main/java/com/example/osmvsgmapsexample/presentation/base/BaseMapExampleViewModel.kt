package com.example.osmvsgmapsexample.presentation.base

import androidx.databinding.ObservableField
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.osmvsgmapsexample.domain.repository.MyDummyRepository
import com.example.osmvsgmapsexample.pojo.CarsharingCar
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.pojo.RouteTurnsInstruction
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment.MapLocation
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment.MapMovementType

abstract class BaseMapExampleViewModel: ViewModel() {

    // Let's just close our eyes and imagine that we've injected it with Dagger
    val repository = MyDummyRepository()

    val userId = 42069L
    var areZoomedFirstTime = false

    val areUserGrantedLocation = MutableLiveData<Boolean>().apply { value = false }

    // navigation
    val navigationInstructions = MediatorLiveData<List<RouteTurnsInstruction>>()
    val navigationRoute = MediatorLiveData<List<MapLocation>>()
    val navigationErrorOccured = MutableLiveData<Boolean>()

    val availableCars = MediatorLiveData<List<CarsharingCar>>()
    val userInCar = MutableLiveData<CarsharingCar>()
    val closestCar = MutableLiveData<CarsharingCar?>()

    val routeDestinationPoint = MutableLiveData<MapLocation>()

    val isCarOccupied = MediatorLiveData<Boolean>()
    val isCarLeft = MediatorLiveData<Boolean>()

    val searchingLocationName = ObservableField<String>()

    // name to coords (many)
    val directGeocodingResult = MediatorLiveData<List<GeocodingResult>>()

    // coords to name (let's take just one)
    val reverseGeocodingResult = MediatorLiveData<GeocodingResult>()

    fun loadAvailableCars() {
        availableCars.addSource(repository.getAvailableCars()) { availableCars.postValue(it) }
    }

    fun occupyCar(car: CarsharingCar) {
        isCarOccupied.addSource(repository.occupyCar(car.id, userId)) {
            if (it) userInCar.value = car
            isCarOccupied.postValue(it)
        }
    }

    fun leaveCurrentUserCar() {
        userInCar.value?.let {
            isCarLeft.addSource(repository.leaveCar(userId, it.lat, it.lng)) {
                if (it) userInCar.value = null
                isCarOccupied.postValue(!it)
            }
        } ?: isCarLeft.postValue(false)
    }

    abstract fun requestRoute(startLocation: MapLocation, vararg pointsOfRoute: MapLocation, movementType: MapMovementType)

    abstract fun requestDirectGeocodingRequestSearch(locationName: String)

    fun requestDirectGeocodingCurrentRequestSearch() = searchingLocationName.get()
        ?.let { requestDirectGeocodingRequestSearch(it) }

    abstract fun requestReverseGeocodingRequestSearch(location: MapLocation)
}