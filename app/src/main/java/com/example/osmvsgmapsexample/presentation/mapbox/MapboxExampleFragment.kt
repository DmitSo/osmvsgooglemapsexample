package com.example.osmvsgmapsexample.presentation.mapbox

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.os.Looper.getMainLooper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.databinding.FragmentMapboxBinding
import com.example.osmvsgmapsexample.pojo.CarsharingCar
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.pojo.RouteTurnsInstruction
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment
import com.example.osmvsgmapsexample.presentation.main.notifyChange
import com.example.osmvsgmapsexample.presentation.main.toMapLocation
import com.example.osmvsgmapsexample.presentation.main.toMapLocationWithToDegreesConvert
import com.example.osmvsgmapsexample.presentation.main.toMapboxLatLng
import com.example.osmvsgmapsexample.util.getBitmap
import com.example.osmvsgmapsexample.util.hideSoftKeyboard
import com.mapbox.android.core.location.*
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationUpdate
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.*
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.utils.ColorUtils
import java.lang.ref.WeakReference


class MapboxExampleFragment : BaseMapExampleFragment(), OnMapReadyCallback {

    lateinit var binding: FragmentMapboxBinding

    private var carMarkers = hashMapOf<Symbol, Any?>()

    private var currentCarMarker: Pair<Symbol, Any?>? = null

    private var map: MapboxMap? = null
    private lateinit var carsSymbolManager: SymbolManager
    private lateinit var geocodingResultSymbolManager: SymbolManager
    private lateinit var routePolylineManager: LineManager

    private var currentDirectGeocodingResultMarker: Symbol? = null
    private var currentReverseGeocodingResultMarker: Symbol? = null
    private var routeLine: Line? = null

    // Location manager are used to observe user's location
    //private var locationManager: LocationManager? = null
    private var locationEngine: LocationEngine? = null
    private val callback = MapboxFragmentLocationCallback(this)

    // Last known location fetched by locationManager
    // would be null if no access has been granted or no location found
    private var lastKnownLocation: Location? = null

    private val stateLeaveCar = R.string.leave_car_button_text to {
        leaveCar()
    }
    private val stateSitInCar = R.string.sit_in_car_button_text to {
        // sit in car if we're close enough
        viewModel.closestCar.value?.let { sitInCar(it) }
        Unit
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token))
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mapbox, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPrimaryButtonState()
        initSearchingFeature()
        binding.viewModel = viewModel as MapboxExampleViewModel

        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.getMapAsync(this)
    }

    private fun initPrimaryButtonState() {
        val gmapsViewModel = viewModel as MapboxExampleViewModel
        if (gmapsViewModel.bottomButtonStatus.get() == null)
            gmapsViewModel.bottomButtonStatus.set(stateSitInCar)

        viewModel.userInCar.observe(viewLifecycleOwner) {
            gmapsViewModel.bottomButtonStatus.set(if (it == null) stateSitInCar else stateLeaveCar)
        }

        binding.primaryButton.setOnClickListener {
            gmapsViewModel.bottomButtonStatus.get()?.second?.invoke()
        }
    }

    private fun initSearchingFeature() {
        binding.clearSearchButton.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            viewModel.searchingLocationName.set("")
            viewModel.directGeocodingResult.postValue(null)
        }
        binding.searchEdit.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                binding.isSearching = true
                hideSoftKeyboard(requireActivity())
                viewModel.requestDirectGeocodingCurrentRequestSearch()
            }
            true
        }
        binding.searchResultsRecycler.adapter = searchingResultsListAdapter
        binding.searchResultsRecycler.itemAnimator = null
    }

    override fun customizeMap() {
        with(map!!) {
            // For custom map style -> Style.Builder().fromUri("mapbox://styles/mapbox/cjf4m44iw0uza2spb3q0a7s41")
            setStyle(Style.LIGHT) { style ->
                setupImagesForStyle(style)

                // Create symbol manager object.
                carsSymbolManager = SymbolManager(binding.mapView, map!!, style)
                geocodingResultSymbolManager = SymbolManager(binding.mapView, map!!, style)
                routePolylineManager = LineManager(binding.mapView, map!!, style)

                map!!.addOnMapClickListener { onMapClick(it.toMapLocation()); true } // true if handled
                map!!.addOnMapLongClickListener {
                    requestReverseGeocodingOnLongClick(it.toMapLocation())
                    true
                }

                carsSymbolManager.addClickListener {
                    val tag = carMarkers[it] ?: currentCarMarker?.second
                    if (tag is CarsharingCar) {
                        onCarClick(tag)
                        true
                    } else false
                }

                geocodingResultSymbolManager.addClickListener {
                    val tag = carMarkers[it] ?: currentCarMarker?.second
                    if (tag is GeocodingResult) {
                        map!!.animateCamera(
                            CameraUpdateFactory.newLatLng(
                                LatLng(tag.lat, tag.lng)
                            )
                        )
                        false
                    } else false
                }

                // Set non-data-driven properties.
                carsSymbolManager.iconAllowOverlap = true
                carsSymbolManager.textAllowOverlap = true
                carsSymbolManager.iconIgnorePlacement = true
                carsSymbolManager.iconTranslate = arrayOf(-4f, 5f)
                carsSymbolManager.iconRotationAlignment = Property.ICON_ROTATION_ALIGNMENT_VIEWPORT

                viewModel.areUserGrantedLocation.observe(viewLifecycleOwner) {
                    if (it) {
                        afterPermsResolved()
                    }
                }
                viewModel.loadAvailableCars()
            }

            uiSettings.setAllGesturesEnabled(true)
            uiSettings.isCompassEnabled = true
            uiSettings.setCompassFadeFacingNorth(true)
            uiSettings.setCompassMargins(
                16,
                binding.searchEdit.y.toInt() + binding.searchEdit.height + 16,
                16,
                16
            )
        }
    }

    private fun setupImagesForStyle(style: Style) {
        style.addImage(
            CAR_IMAGE_TAG,
            getBitmap(R.drawable.ic_car, android.R.color.black)!!
        )
        style.addImage(
            GEOCODING_IMAGE_TAG,
            getBitmap(R.drawable.ic_baseline_location, android.R.color.black)!!
        )
    }

    /**
     * Initialize the Maps SDK's LocationComponent
     * should call after location permissions are granted
     */
    @SuppressWarnings("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Get an instance of the component
        val locationComponent: LocationComponent = map!!.locationComponent

        // Set the LocationComponent activation options
        val locationComponentActivationOptions =
            LocationComponentActivationOptions.builder(requireContext(), loadedMapStyle)
                .useDefaultLocationEngine(false)
                .build()

        // Activate with the LocationComponentActivationOptions object
        locationComponent.activateLocationComponent(locationComponentActivationOptions)

        // Enable to make component visible
        locationComponent.isLocationComponentEnabled = true

        // Set the component's camera mode
        locationComponent.cameraMode = CameraMode.TRACKING

        // Set the component's render mode
        locationComponent.renderMode = RenderMode.COMPASS

        initLocationEngine()
    }

    override fun onReverseGeocodingResult(result: GeocodingResult?) {
        result?.let { currentReverseGeocodingResultMarker = addGeocodingResultMarker(it) }
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this).get(MapboxExampleViewModel::class.java)
        (viewModel as MapboxExampleViewModel).googleApiKey = getString(R.string.gmaps_api_key)
        (viewModel as MapboxExampleViewModel).mapboxAccessToken = getString(R.string.mapbox_access_token)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        map = mapboxMap
        customizeMap()
    }

    @SuppressLint("MissingPermission")
    override fun afterPermsResolved() {
        enableLocationComponent(map!!.style!!);
    }

    override fun drawAvailableCars(values: List<CarsharingCar>) {
        if (this::carsSymbolManager.isInitialized) {
            carsSymbolManager.deleteAll()
            carMarkers = hashMapOf(*values.map { addCarMarker(it) }.toTypedArray())
        }
    }

    // if you need to apply color on marker you should convert it to SDF format:
    // https://stackoverflow.com/questions/63299999/how-can-i-create-sdf-icons-used-in-mapbox-from-png
    private fun addCarMarker(car: CarsharingCar) = carsSymbolManager.create(
        SymbolOptions()
            .withIconImage(CAR_IMAGE_TAG)
            .withDraggable(false)
            .withLatLng(LatLng(car.lat, car.lng))
            .withIconAnchor(Property.ICON_ANCHOR_CENTER)
    ) to car

    protected fun addGeocodingResultMarker(result: GeocodingResult) = carsSymbolManager.create(
        SymbolOptions()
            .withIconImage(GEOCODING_IMAGE_TAG)
            .withDraggable(false)
            .withLatLng(LatLng(result.lat, result.lng))
    )

    override fun removeUserCarMarkerFromMap() {
        if (this::carsSymbolManager.isInitialized) {
            currentCarMarker?.let { carsSymbolManager.delete(it.first) }
        }
    }

    override fun placeUserCarMarkerOnMap(car: CarsharingCar) {
        currentCarMarker = addCarMarker(car)
    }

    @SuppressLint("MissingPermission")
    override fun removeUserLocationMarkerFromMap() {
        map?.locationComponent?.isLocationComponentEnabled = false
    }

    @SuppressLint("MissingPermission")
    override fun placeOrUpdateUserLocationMarkerOnMap() {
        map?.locationComponent?.isLocationComponentEnabled = true
    }

    override fun onSearchResultClick(result: GeocodingResult) {
        currentDirectGeocodingResultMarker?.let { geocodingResultSymbolManager.delete(it) }

        currentDirectGeocodingResultMarker = addGeocodingResultMarker(result)
        map?.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(result.lat, result.lng),
                DEFAULT_SEARCH_RESULT_ZOOM.toDouble()
            )
        )
    }

    override fun moveCurrentCarMarkerToLocation(location: MapLocation, withDegrees: Double?) {
        currentCarMarker?.first?.let { marker ->
            marker.latLng = location.toMapboxLatLng()
            withDegrees?.let {
                marker.iconRotate = it.toFloat() - 180 // => minus drawable rotation itself
            }
            carsSymbolManager.update(marker)
        }
    }

    override fun zoomOnUserLocation(zoomLevel: Float) {
        val userLocation = getUserMapLocation()?.toMapboxLatLng()
        userLocation?.let {
            map?.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    it,
                    DEFAULT_WALKING_ZOOM_LEVEL.toDouble()
                )
            )
        }
    }

    override fun onDestinationPointChosen(mapLocation: MapLocation) {
        val startLocation = getUserMapLocation()
        startLocation?.let {
            val movementType =
                if (viewModel.userInCar.value == null) MapMovementType.WALKING else MapMovementType.DRIVING
            viewModel.requestRoute(it, mapLocation, movementType = movementType)
        }
    }

    override fun drawRouteInstructions(instructions: List<RouteTurnsInstruction>) {

    }

    override fun drawRoutePolyline(line: List<MapLocation>, inErrorState: Boolean?) {
        viewModel.userInCar.notifyChange()
        routeLine?.let { routePolylineManager.delete(it) }
        val polylineOption = getAccordingPolylineOption(inErrorState)
        polylineOption.withLatLngs(line.map { it.toMapboxLatLng() })
        routeLine = routePolylineManager.create(polylineOption)
    }

    protected fun getAccordingPolylineOption(areErrorOccured: Boolean?) =
        when (areErrorOccured) {
            true -> getErrorStyledPolyline()
            false -> getUsualStyledPolyline()
            null -> getBadRouteStyledPolyline()
        }

    protected fun getUsualStyledPolyline() = LineOptions()
        .withLineWidth(DEFAULT_LINE_WIDTH_PX)
        .withLinePattern("airfield-11")
        .withLineColor(
            ColorUtils.colorToRgbaString(
                ResourcesCompat.getColor(
                    resources,
                    R.color.color_map_route_ordinary,
                    context?.theme
                )
            )
        )

    protected fun getErrorStyledPolyline() = LineOptions()
        .withLineWidth(DEFAULT_LINE_WIDTH_PX)
        .withLineColor(
            ColorUtils.colorToRgbaString(
                ResourcesCompat.getColor(resources, R.color.color_map_bad_route, context?.theme)
            )
        )

    protected fun getBadRouteStyledPolyline() = LineOptions()
        .withLineWidth(DEFAULT_LINE_WIDTH_PX)
        .withLinePattern("airfield-11")
        .withLineColor(
            ColorUtils.colorToRgbaString(
                ResourcesCompat.getColor(resources, R.color.color_map_bad_route, context?.theme)
            )
        )

    override fun onCancelRouteBuild() {
        routeLine?.let { routePolylineManager.delete(it) }
        viewModel.availableCars.notifyChange()
    }

    override fun getDistanceBetweenInMeters(l1: MapLocation, l2: MapLocation): Double =
        getDistanceBetweenInMeters(l1.toMapboxLatLng(), l2.toMapboxLatLng())

    private fun getDistanceBetweenInMeters(l1: LatLng, l2: LatLng): Double =
        l1.distanceTo(l2)

    override fun getUserMapLocation(): MapLocation? =
        lastKnownLocation?.toMapLocationWithToDegreesConvert()

    override fun setSitInCarActionAvailable(isAvailable: Boolean) {
        binding.primaryButton.visibility = if (isAvailable) View.VISIBLE else View.GONE
    }

    /**
     * Set up the LocationEngine and the parameters for querying the device's location
     */
    @SuppressLint("MissingPermission")
    private fun initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(requireContext())
        val request = LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
            .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build()
        locationEngine!!.requestLocationUpdates(request, callback, getMainLooper())
        locationEngine!!.getLastLocation(callback)
    }

    // MapView itself have a lifecycle methods we should call to made it work properly

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        binding.mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.mapView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.mapView.onDestroy()
        locationEngine?.removeLocationUpdates(callback) // Prevent leaks
    }

    override fun onDestroyView() {
        binding.mapView.onDestroy()
        super.onDestroyView()
    }

    /** Custom location callback for this fragment
     */
    private inner class MapboxFragmentLocationCallback(mapboxFragment: MapboxExampleFragment?) :
        LocationEngineCallback<LocationEngineResult> {
        private val fragmentWeakReference: WeakReference<MapboxExampleFragment> =
            WeakReference(mapboxFragment)

        /**
         * The LocationEngineCallback interface's method which fires when the device's location has changed.
         *
         * @param result the LocationEngineResult object which has the last known location within it.
         */
        override fun onSuccess(result: LocationEngineResult) {
            fragmentWeakReference.get()?.let { fragment ->
                val location = result.lastLocation ?: return

                fragment.lastKnownLocation = location

                // Pass the new location to the Maps SDK's LocationComponent
                fragment.map?.locationComponent
                    ?.forceLocationUpdate(
                        LocationUpdate.Builder()
                            .location(result.lastLocation)
                            .build()
                    )
            }
        }

        /**
         * The LocationEngineCallback interface's method which fires when the device's location can not be captured
         *
         * @param exception the exception message
         */
        override fun onFailure(exception: Exception) {
            Log.d(DEBUG_TAG, exception.localizedMessage ?: exception.message ?: "")
        }
    }

    companion object {
        private val DEBUG_TAG = MapboxExampleFragment::class.java.name
        private const val CAR_IMAGE_TAG = "CAR_IMAGE_TAG"
        private const val GEOCODING_IMAGE_TAG = "GEOCODING_IMAGE_TAG"

        private const val DEFAULT_INTERVAL_IN_MILLISECONDS = 3000L
        private const val DEFAULT_MAX_WAIT_TIME = 4 * DEFAULT_INTERVAL_IN_MILLISECONDS

        private const val DEFAULT_LINE_WIDTH_PX = 7f

        const val MAX_GEOCODING_RESULTS = 3
    }
}