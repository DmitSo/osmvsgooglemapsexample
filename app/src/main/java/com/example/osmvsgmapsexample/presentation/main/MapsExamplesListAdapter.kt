package com.example.osmvsgmapsexample.presentation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.databinding.ItemMapsExampleBinding

class MapsExamplesListAdapter :
    RecyclerView.Adapter<MapsExamplesListAdapter.MapsExampleItemViewHolder>() {

    var items: List<MapsExampleItem> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MapsExampleItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemMapsExampleBinding>(inflater, R.layout.item_maps_example, parent, false)
        return MapsExampleItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MapsExampleItemViewHolder, position: Int) {
        val item = items[position]
        with(holder.binding) {
            exampleNameRes = item.nameRes
            exampleImageRes = item.imageRes
            root.setOnClickListener { item.onChoosedAction() }
        }
    }

    override fun getItemCount(): Int = items.size

    inner class MapsExampleItemViewHolder(val binding: ItemMapsExampleBinding) :
        RecyclerView.ViewHolder(binding.root)
}