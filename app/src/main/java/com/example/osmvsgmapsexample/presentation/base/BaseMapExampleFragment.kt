package com.example.osmvsgmapsexample.presentation.base

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.pojo.CarsharingCar
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.pojo.RouteTurnsInstruction
import com.example.osmvsgmapsexample.presentation.main.getMapLocation
import com.example.osmvsgmapsexample.presentation.main.notifyChange
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import kotlin.math.atan

abstract class BaseMapExampleFragment : Fragment() {

    protected lateinit var viewModel: BaseMapExampleViewModel

    private var isUserSitInCarTimerOn: Boolean = true
    private var isCarMovementTimerOn: Boolean = true

    protected val searchingResultsListAdapter = BaseDirectGeocodingSearchResultsListAdapter()

    protected var areUserLocationShown: Boolean = false
    set(value) {
        field = value
        if (value) placeOrUpdateUserLocationMarkerOnMap()
        else removeUserLocationMarkerFromMap()
    }

    open val DEFAULT_WALKING_ZOOM_LEVEL = 16.0f
    open val DEFAULT_DRIVING_ZOOM_LEVEL = 12.0f
    open val DEFAULT_SEARCH_RESULT_ZOOM = 14.0f

    abstract override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View?

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        observeLiveDatas()
        searchingResultsListAdapter.onResultClick = { item ->
            viewModel.directGeocodingResult.postValue(null)
            onSearchResultClick(item)
        }
    }

    abstract fun customizeMap()

    override fun onResume() {
        super.onResume()
        resolvePermissions()
        viewModel.userInCar.notifyChange()
    }

    override fun onPause() {
        super.onPause()
        disarmSitInCarTimer()
        disarmCarMovementTimer()
    }

    private fun rearmSitInCarTimer() {
        isUserSitInCarTimerOn = true
        zoomOnUserLocation(DEFAULT_DRIVING_ZOOM_LEVEL)
        startSitInCarTimer()
    }

    private fun rearmCarMovementTimer() {
        isCarMovementTimerOn = true
        startCarMovementTimer()
    }

    private fun disarmSitInCarTimer() {
        isUserSitInCarTimerOn = false
    }

    private fun disarmCarMovementTimer() {
        isCarMovementTimerOn = false
    }

    fun observeLiveDatas() {
        viewModel.availableCars.observe(viewLifecycleOwner) {
            drawAvailableCars(it ?: listOf())
        }
        viewModel.userInCar.observe(viewLifecycleOwner) {
            if (it == null) {
                areUserLocationShown = true
                removeUserCarMarkerFromMap()
                rearmSitInCarTimer()
                viewModel.loadAvailableCars()
            } else {
                areUserLocationShown = false
                disarmSitInCarTimer()
                rearmCarMovementTimer()
                viewModel.closestCar.postValue(null)
                viewModel.availableCars.postValue(null)
                placeUserCarMarkerOnMap(viewModel.userInCar.value!!)
            }
        }
        viewModel.routeDestinationPoint.observe(viewLifecycleOwner) {
            if (it == null) {
                onCancelRouteBuild()
            } else {
                onDestinationPointChosen(it)
            }
        }
        viewModel.navigationInstructions.observe(viewLifecycleOwner) {
            drawRouteInstructions(it)
        }
        viewModel.navigationErrorOccured.observe(viewLifecycleOwner) { isError ->
            viewModel.navigationRoute.value?.let {
                drawRoutePolyline(it, isError)
            }
        }
        viewModel.closestCar.observe(viewLifecycleOwner) {
            setSitInCarActionAvailable(
                viewModel.userInCar.value == null && it != null // state - searching for car
                    || viewModel.userInCar.value != null        // state - in car (can leave)
            )
        }
        viewModel.isCarOccupied.observe(viewLifecycleOwner) {  }
        viewModel.isCarLeft.observe(viewLifecycleOwner) {  }
        viewModel.navigationRoute.observe(viewLifecycleOwner) {  }
        viewModel.directGeocodingResult.observe(viewLifecycleOwner) {
            searchingResultsListAdapter.items = it ?: listOf()
        }
        viewModel.reverseGeocodingResult.observe(viewLifecycleOwner) {
            onReverseGeocodingResult(it)
        }
    }

    abstract fun onReverseGeocodingResult(result: GeocodingResult?)

    abstract fun removeUserCarMarkerFromMap()
    abstract fun placeUserCarMarkerOnMap(car: CarsharingCar)

    abstract fun removeUserLocationMarkerFromMap()
    abstract fun placeOrUpdateUserLocationMarkerOnMap() // Would use lastKnownLocation

    // move on map and place a marker
    abstract fun onSearchResultClick(result: GeocodingResult)

    // updates availability of sitting the user in a car
    private fun startSitInCarTimer() {
        CoroutineScope(Dispatchers.Main).launch {
            while (isUserSitInCarTimerOn) {
                if (!viewModel.areZoomedFirstTime) {
                    zoomOnUserLocation(DEFAULT_WALKING_ZOOM_LEVEL)
                    viewModel.areZoomedFirstTime = true
                }

                viewModel.availableCars.value?.let { cars ->
                    val userLocation = getUserMapLocation()
                    userLocation?.let {
                        val closestCar = findClosestCar(it, cars)
                        val areSittingInCarAvailable =
                            closestCar.second < USER_APPROACH_TO_CAR_DISTANCE_METERS
                        viewModel.closestCar.postValue(if (areSittingInCarAvailable) closestCar.first else null)
                    }
                }

                if (areUserLocationShown) placeOrUpdateUserLocationMarkerOnMap()

                delay(USER_SIT_IN_CAR_TIMER_UPDATE_RATE_MS)
            }
        }
    }

    // updates availability of sitting the user in a car
    private fun startCarMovementTimer() {
        CoroutineScope(Dispatchers.Main).launch {
            while (isCarMovementTimerOn) {
                viewModel.userInCar.value?.let { car ->
                    getUserMapLocation()?.let { userLocation ->
                        val carLocation = car.getMapLocation()
                        val deltaDegrees = if (getDistanceBetweenInMeters(carLocation, userLocation) > ACCURACY_DELTA_METERS)
                            calculateDeltaDegrees(carLocation, userLocation)
                        else null

                        moveCurrentCarMarkerToLocation(userLocation, deltaDegrees)
                        car.lat = userLocation.lat
                        car.lng = userLocation.lng
                    }
                }
                delay(USER_CAR_MOVEMENT_TIMER_UPDATE_RATE_MS)
            }
        }
    }

    private fun calculateDeltaDegrees(l1: MapLocation, l2: MapLocation): Double {
        // get vector
        val vector = MapLocation(l2.lat - l1.lat, l2.lng - l1.lng)
        val result = Math.toDegrees(atan(vector.lng/vector.lat))
        return if (result.isFinite()) result else 0.0
    }

    abstract fun moveCurrentCarMarkerToLocation(location: MapLocation, withDegrees: Double? = null)

    abstract fun zoomOnUserLocation(zoomLevel: Float)

    // on map click when we are in driving mode
    fun onMapClick(location: MapLocation) {
        if (viewModel.userInCar.value != null) {
            viewModel.routeDestinationPoint.postValue(location)
        } else {
            onCancelRouteBuild()
        }
    }

    fun requestReverseGeocodingOnLongClick(location: MapLocation) {
        viewModel.requestReverseGeocodingRequestSearch(location)
    }

    // on "Cancel build route" button click
    fun onCancelRouteClick() {
        viewModel.routeDestinationPoint.postValue(null)
    }

    // appear "Build Route" button on UI
    abstract fun onDestinationPointChosen(mapLocation: MapLocation)

    // call it on "Build route" button click
    fun requestRoute(mapLocation: MapLocation) {
        getUserMapLocation()?.let { userLocation ->
            val movementType =
                if (viewModel.userInCar.value == null) MapMovementType.WALKING
                else MapMovementType.DRIVING
            viewModel.requestRoute(userLocation, mapLocation, movementType = movementType)
        }
    }

    // called when we want to build route to the car
    fun onCarClick(car: CarsharingCar) {
        if (viewModel.userInCar.value == null) {
            viewModel.routeDestinationPoint.postValue(MapLocation(car.lat, car.lng))
        }
    }

    // requires to build the route instructions and show it in view
    abstract fun drawRouteInstructions(instructions: List<RouteTurnsInstruction>)

    // draws route on map
    abstract fun drawRoutePolyline(line: List<MapLocation>, inErrorState: Boolean?)

    // clear map
    abstract fun onCancelRouteBuild()

    private fun findClosestCar(location: MapLocation, cars: List<CarsharingCar>) = cars
        .map { it to getDistanceBetweenInMeters(location, it.getMapLocation()) }
        .sortedBy { it.second }
        .first()

    abstract fun getDistanceBetweenInMeters(l1: MapLocation, l2: MapLocation): Double

    abstract fun initViewModel()

    @AfterPermissionGranted(LOCATION_PERMS_REQUEST_CODE)
    fun resolvePermissions() {
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (checkIfAllPermissionsGranted(*permissions)) {
            viewModel.areUserGrantedLocation.postValue(true)
        } else {
            EasyPermissions.requestPermissions(
                PermissionRequest.Builder(this, LOCATION_PERMS_REQUEST_CODE, *permissions)
                    .setRationale(R.string.location_rationale_text)
                    .build()
            )
        }
    }

    private fun checkIfAllPermissionsGranted(vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        return true
    }

    abstract fun afterPermsResolved()

    abstract fun drawAvailableCars(values: List<CarsharingCar>)

    abstract fun getUserMapLocation(): MapLocation?

    abstract fun setSitInCarActionAvailable(isAvailable: Boolean)

    fun sitInCar(car: CarsharingCar) {
        onCancelRouteBuild()
        viewModel.occupyCar(car)
    }

    fun leaveCar() {
        onCancelRouteBuild()
        viewModel.leaveCurrentUserCar()
    }

    class MapLocation(val lat: Double, val lng: Double)

    enum class MapMovementType {
        WALKING,
        DRIVING
    }

    companion object {
        private const val USER_SIT_IN_CAR_TIMER_UPDATE_RATE_MS = 2 * 1000L
        private const val USER_CAR_MOVEMENT_TIMER_UPDATE_RATE_MS = 2 * 1000L
        private const val USER_APPROACH_TO_CAR_DISTANCE_METERS = 20

        private const val LOCATION_PERMS_REQUEST_CODE = 999

        private const val ACCURACY_DELTA_METERS = 10
    }
}