package com.example.osmvsgmapsexample.presentation.gmaps

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.databinding.FragmentGoogleMapsBinding
import com.example.osmvsgmapsexample.pojo.CarsharingCar
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.pojo.RouteTurnsInstruction
import com.example.osmvsgmapsexample.pojo.getMapLocation
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment
import com.example.osmvsgmapsexample.presentation.main.bitmapDescriptorFromVector
import com.example.osmvsgmapsexample.presentation.main.notifyChange
import com.example.osmvsgmapsexample.presentation.main.toGoogleMapsLatLng
import com.example.osmvsgmapsexample.presentation.main.toMapLocationWithToDegreesConvert
import com.example.osmvsgmapsexample.util.getLastKnownLocationWithTriggering
import com.example.osmvsgmapsexample.util.hideSoftKeyboard
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil
import java.util.*


class GoogleMapsExampleFragment : BaseMapExampleFragment(), OnMapReadyCallback {

    lateinit var binding: FragmentGoogleMapsBinding

    private var carMarkers = mutableListOf<Marker>()

    private var currentCarMarker: Marker? = null

    private var currentDirectGeocodingResultMarker: Marker? = null
    private var currentReverseGeocodingResultMarker: Marker? = null

    // Used to work with the map
    private lateinit var map: GoogleMap

    // Location manager are used to observe user's location
    private var locationManager: LocationManager? = null

    // Last known location fetched by locationManager
    // would be null if no access has been granted
    private val lastKnownLocation: Location?
        @SuppressLint("MissingPermission") get() = locationManager?.let { getLastKnownLocation(it) }

    private val stateLeaveCar = R.string.leave_car_button_text to {
        leaveCar()
    }
    private val stateSitInCar = R.string.sit_in_car_button_text to {
        // sit in car if we're close enough
        viewModel.closestCar.value?.let { sitInCar(it) }
        Unit
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_google_maps, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPrimaryButtonState()
        initSearchingFeature()
        binding.viewModel = viewModel as GoogleMapsExampleViewModel

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map_view) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun initPrimaryButtonState() {
        val gmapsViewModel = viewModel as GoogleMapsExampleViewModel
        if (gmapsViewModel.bottomButtonStatus.get() == null)
            gmapsViewModel.bottomButtonStatus.set(stateSitInCar)

        viewModel.userInCar.observe(viewLifecycleOwner) {
            gmapsViewModel.bottomButtonStatus.set(if (it == null) stateSitInCar else stateLeaveCar)
        }

        binding.primaryButton.setOnClickListener {
            gmapsViewModel.bottomButtonStatus.get()?.second?.invoke()
        }
    }

    private fun initSearchingFeature() {
        binding.clearSearchButton.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            viewModel.searchingLocationName.set("")
            viewModel.directGeocodingResult.postValue(null)
        }
        binding.searchEdit.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                binding.isSearching = true
                hideSoftKeyboard(requireActivity())
                viewModel.requestDirectGeocodingCurrentRequestSearch()
            }
            true
        }
        binding.searchResultsRecycler.adapter = searchingResultsListAdapter
        binding.searchResultsRecycler.itemAnimator = null

        // direct geocoding
        (viewModel as GoogleMapsExampleViewModel).directGeocodingKostilForSearching.observe(
            viewLifecycleOwner
        ) {
            binding.isSearching = false
            val result = Geocoder(requireContext(), Locale.getDefault()).getFromLocationName(
                it,
                MAX_GEOCODING_RESULTS
            )
            viewModel.directGeocodingResult.postValue(result.map {
                GeocodingResult(
                    it.getAddressLine(
                        it.maxAddressLineIndex
                    ), it.latitude, it.longitude
                )
            })
        }

        // reverse geocoding
        (viewModel as GoogleMapsExampleViewModel).reverseGeocodingKostilForSearching.observe(
            viewLifecycleOwner
        ) {
            val result = Geocoder(requireContext(), Locale.getDefault()).getFromLocation(
                it.lat,
                it.lng,
                1
            )
            val oneResult = result[0]
            viewModel.reverseGeocodingResult.postValue(
                GeocodingResult(
                    oneResult.getAddressLine(
                        oneResult.maxAddressLineIndex
                    ), oneResult.latitude, oneResult.longitude
                )
            )
        }
    }

    override fun customizeMap() {
        with(map) {
            setPadding(16, binding.searchEdit.y.toInt() + binding.searchEdit.height + 16, 16, 16)

            uiSettings.setAllGesturesEnabled(true)
            uiSettings.isMyLocationButtonEnabled = true
            uiSettings.isMapToolbarEnabled = false

            mapType = GoogleMap.MAP_TYPE_NORMAL

            // Mofify styles at https://mapstyle.withgoogle.com
            val success: Boolean = setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    requireContext(), R.raw.raw_night_style_gmaps
                )
            )
            if (!success) Toast.makeText(requireContext(), "Bad styling", Toast.LENGTH_SHORT).show()
        /*map.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoWindow(p0: Marker?): View {
            }

            override fun getInfoContents(p0: Marker?): View {
            }
        })*/
        }
    }

    override fun onReverseGeocodingResult(result: GeocodingResult?) {
        result?.let { currentReverseGeocodingResultMarker = addGeocodingResultMarker(it) }
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this).get(GoogleMapsExampleViewModel::class.java)
        (viewModel as GoogleMapsExampleViewModel).googleApiKey = getString(R.string.gmaps_api_key)
    }

    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        customizeMap()

        googleMap.setOnMapClickListener { onMapClick(it.toMapLocationWithToDegreesConvert()) }
        googleMap.setOnMapLongClickListener { requestReverseGeocodingOnLongClick(it.toMapLocationWithToDegreesConvert()) }
        googleMap.setOnMarkerClickListener {
            if (it?.tag is CarsharingCar) {
                onCarClick(it.tag as CarsharingCar)
                true
            } else if (it?.tag is GeocodingResult) {
                val result = it.tag as GeocodingResult
                map.animateCamera(CameraUpdateFactory.newLatLng(LatLng(result.lat, result.lng)))
                false
            } else false
        }

        viewModel.areUserGrantedLocation.observe(viewLifecycleOwner) {
            if (it) { afterPermsResolved() }
        }
        viewModel.loadAvailableCars()
    }

    @SuppressLint("MissingPermission")
    override fun afterPermsResolved() {
        map.isMyLocationEnabled = true
        activity?.let {
            locationManager = it.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        }
    }

    override fun drawAvailableCars(values: List<CarsharingCar>) {
        carMarkers.forEach { v -> v.isVisible = false }
        carMarkers = values.map { addCarMarker(it) }.toMutableList()
    }

    private fun addCarMarker(car: CarsharingCar) = map.addMarker(
        MarkerOptions()
            .position(car.getMapLocation().toGoogleMapsLatLng())
            .icon(
                bitmapDescriptorFromVector(
                    requireContext(),
                    R.drawable.ic_car,
                    android.R.color.black//Color.parseColor(car.colorHexString)
                )
            )
            .title("${car.colorName} ${car.markkaAndModelString}")
            .snippet(car.vehicleNumber)
            .draggable(false)
            .flat(true)         // Закреплен ли маркер
            .anchor(0.5f, 0.5f) // anchor on center of the marker
        //.rotation(180f)       // Rotate marker
        //.alpha(0.5f)      // Alpha in range 0..1
        //.visible(false)
        //.zIndex()
    ).apply {
        tag = car
    }

    protected fun addGeocodingResultMarker(result: GeocodingResult) = map.addMarker(
        MarkerOptions()
            .position(LatLng(result.lat, result.lng))
            .title(result.formattedName)
    ).apply {
        tag = result
    }

    override fun removeUserCarMarkerFromMap() {
        currentCarMarker?.isVisible = false
    }

    override fun placeUserCarMarkerOnMap(car: CarsharingCar) {
        currentCarMarker = addCarMarker(car)
    }

    @SuppressLint("MissingPermission")
    override fun removeUserLocationMarkerFromMap() {
        if (this::map.isInitialized) map.isMyLocationEnabled = false
    }

    @SuppressLint("MissingPermission")
    override fun placeOrUpdateUserLocationMarkerOnMap() {
        if (this::map.isInitialized) map.isMyLocationEnabled = true
    }

    override fun onSearchResultClick(result: GeocodingResult) {
        currentDirectGeocodingResultMarker?.remove()

        currentDirectGeocodingResultMarker = addGeocodingResultMarker(result)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(result.lat, result.lng),
                DEFAULT_SEARCH_RESULT_ZOOM
            )
        )
    }

    override fun moveCurrentCarMarkerToLocation(location: MapLocation, withDegrees: Double?) {
        currentCarMarker?.let { marker ->
            marker.position = location.toGoogleMapsLatLng()
            withDegrees?.let {
                marker.rotation = it.toFloat() - 180 // => minus drawable rotation itself
            }
        }
    }

    override fun zoomOnUserLocation(zoomLevel: Float) {
        val userLocation = getUserMapLocation()?.toGoogleMapsLatLng()
        userLocation?.let {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(it, DEFAULT_WALKING_ZOOM_LEVEL))
        }
    }

    override fun onDestinationPointChosen(mapLocation: MapLocation) {
        val startLocation = getUserMapLocation()
        startLocation?.let {
            val movementType = if (viewModel.userInCar.value == null) MapMovementType.WALKING else MapMovementType.DRIVING
            viewModel.requestRoute(it, mapLocation, movementType = movementType)
        }
    }

    override fun drawRouteInstructions(instructions: List<RouteTurnsInstruction>) {

    }

    override fun drawRoutePolyline(line: List<MapLocation>, inErrorState: Boolean?) {
        map.clear()
        viewModel.userInCar.notifyChange()
        val polylineOptions = getAccordingPolyline(inErrorState)
        polylineOptions.addAll(line.map { it.toGoogleMapsLatLng() })
        map.addPolyline(polylineOptions)
    }

    protected fun getAccordingPolyline(areErrorOccured: Boolean?) =
        when (areErrorOccured) {
            true -> getErrorStyledPolyline()
            false -> getUsualStyledPolyline()
            null -> getBadRouteStyledPolyline()
        }

    protected fun getUsualStyledPolyline() = PolylineOptions()
        .color(requireContext().resources.getColor(R.color.color_map_route_ordinary))
        .width(DEFAULT_LINE_WIDTH_PX)

    protected fun getErrorStyledPolyline() = PolylineOptions()
        .color(requireContext().resources.getColor(R.color.color_map_bad_route))
        .width(DEFAULT_LINE_WIDTH_PX)
        .pattern(POLYGON_LIST_STROKE)

    protected fun getBadRouteStyledPolyline() = PolylineOptions()
        .color(requireContext().resources.getColor(R.color.color_map_bad_route))
        .width(DEFAULT_LINE_WIDTH_PX)

    override fun onCancelRouteBuild() {
        map.clear()
        viewModel.availableCars.notifyChange()
    }

    override fun getDistanceBetweenInMeters(l1: MapLocation, l2: MapLocation): Double =
        getDistanceBetweenInMeters(l1.toGoogleMapsLatLng(), l2.toGoogleMapsLatLng())

    private fun getDistanceBetweenInMeters(l1: LatLng, l2: LatLng): Double =
        SphericalUtil.computeDistanceBetween(l1, l2)

    override fun getUserMapLocation(): MapLocation? = lastKnownLocation?.toMapLocationWithToDegreesConvert()

    override fun setSitInCarActionAvailable(isAvailable: Boolean) {
        binding.primaryButton.visibility = if (isAvailable) View.VISIBLE else View.GONE
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(locationManager: LocationManager): Location? {
        val providers = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val currentProviderLastLocation: Location =
                locationManager.getLastKnownLocationWithTriggering(provider)
                    ?: continue
            if (bestLocation == null || currentProviderLastLocation.accuracy < bestLocation.accuracy) {
                bestLocation = currentProviderLastLocation
            }
        }
        return bestLocation
    }

    companion object {
        private const val DEFAULT_LINE_WIDTH_PX = 7f
        private const val PATTERN_GAP_DASH_LENGTH_PX = 20f
        private val DASH: PatternItem = Dash(PATTERN_GAP_DASH_LENGTH_PX)
        private val GAP: PatternItem = Gap(PATTERN_GAP_DASH_LENGTH_PX)

        private val POLYGON_LIST_STROKE = listOf(GAP, DASH)
        const val MAX_GEOCODING_RESULTS = 3
    }
}