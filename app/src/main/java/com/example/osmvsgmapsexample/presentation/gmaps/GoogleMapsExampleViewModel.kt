package com.example.osmvsgmapsexample.presentation.gmaps

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.example.osmvsgmapsexample.domain.repository.*
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment.MapMovementType.*
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleViewModel
import com.example.osmvsgmapsexample.util.toGoogleApiRequestLocation
import com.example.osmvsgmapsexample.util.toGoogleApiRequestMovementType
import com.example.osmvsgmapsexample.util.toMapLocation

class GoogleMapsExampleViewModel : BaseMapExampleViewModel() {

    val googleDirectionsRepository: RoutesApiRepository = GoogleDirectionsApiRepository() // GoogleDirectionsApiRepository()

    val bottomButtonStatus = ObservableField<Pair<Int, () -> Unit>>()

    var googleApiKey: String? = null
        set(value) {
            field = value
            (googleDirectionsRepository as? GoogleDirectionsApiRepository)?.apiKey = value
        }

    // Geocoding kostili
    val directGeocodingKostilForSearching = MutableLiveData<String>()
    val reverseGeocodingKostilForSearching = MutableLiveData<BaseMapExampleFragment.MapLocation>()

    override fun requestRoute(
        startLocation: BaseMapExampleFragment.MapLocation,
        vararg pointsOfRoute: BaseMapExampleFragment.MapLocation,
        movementType: BaseMapExampleFragment.MapMovementType
    ) {
        navigationRoute.addSource(googleDirectionsRepository.requestRouteFromApi(
            movementType = movementType.toGoogleApiRequestMovementType(),
            startLocation.toGoogleApiRequestLocation(),
            *pointsOfRoute.map { it.toGoogleApiRequestLocation() }.toTypedArray()
        )) {
            navigationRoute.postValue(it.route?.map { it.toMapLocation() })
            navigationErrorOccured.postValue(it.areErrorOccured)
        }
    }

    override fun requestDirectGeocodingRequestSearch(locationName: String) {
        directGeocodingKostilForSearching.postValue(locationName)
    }

    override fun requestReverseGeocodingRequestSearch(location: BaseMapExampleFragment.MapLocation) {
        reverseGeocodingKostilForSearching.postValue(location)
    }
}