package com.example.osmvsgmapsexample.presentation.main

import androidx.annotation.StringRes
import com.example.osmvsgmapsexample.R

data class MapsExampleItem(
    @StringRes val nameRes: Int,
    val imageRes: Int = R.drawable.ic_baseline_map_24,
    val onChoosedAction: () -> Unit
)