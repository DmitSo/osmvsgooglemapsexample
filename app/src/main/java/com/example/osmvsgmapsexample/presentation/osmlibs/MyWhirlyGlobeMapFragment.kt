package com.example.osmvsgmapsexample.presentation.osmlibs

import android.util.Log
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.example.osmvsgmapsexample.BuildConfig
import com.example.osmvsgmapsexample.util.getBitmap
import com.mousebird.maply.*
import okhttp3.Headers
import okhttp3.Headers.Companion.toHeaders
import java.io.File


open class MyWhirlyGlobeMapFragment @JvmOverloads constructor(
    customType: MapCustomizingType = MapCustomizingType.THUNDERFOREST_MAP_TILE_PROVIDER
) : GlobeMapFragment() {

    /* Listeners made for more comfortable work with the class from outside of it */
    var onControlStartedListener: OnWhirlyGlobeControlStartedListener? = null
    var onTapListener: OnWhirlyGlobeTappedListener? = null
    var onSelectListener: OnWhirlyGlobeSelectObjectListener? = null
    var onLongPressListener: OnWhirlyGlobeLongPressListener? = null

    private val componentObjectsForMarkersOnMap = mutableMapOf<ScreenMarker, ComponentObject>()
    private val componentObjectsForVectorsOnMap = mutableMapOf<VectorObject, ComponentObject>()

    private var customizingType = customType
        set(value) {
            field = value
            initMapWithCurrentTileProvider()
        }

    var rotateGestureEnabled: Boolean = false
        set(value) {
            if (chooseDisplayType() == MapDisplayType.Map) {
                field = value
                mapControl.setAllowRotateGesture(rotateGestureEnabled)
            } else Log.w(DEBUG_TAG, "Globe cannot provide rotation of the map")
        }

    // map or 3d globe
    override fun chooseDisplayType(): MapDisplayType = MapDisplayType.Map

    // When map is ready to use
    // Here we are enabling caching and loader for map tiles
    override fun controlHasStarted() {
        initMapWithCurrentTileProvider()

        onControlStartedListener?.onControlStarted()
    }

    private fun initMapWithCurrentTileProvider() {
        // Set up the local cache directory
        val tilesCacheDir = File(requireActivity().cacheDir, customizingType.cacheDirName)
        tilesCacheDir.mkdir()

        // Set up access to the tile images
        val tileInfo = RemoteTileInfoNew(
            customizingType.tileProviderUrl,
            MIN_MAP_ZOOM,
            MAX_MAP_ZOOM
        ).apply {
            cacheDir = tilesCacheDir
            // coordSys = SphericalMercatorCoordSystem()
            headers = customizingType.headers.toHeaders()
        }

        // Set up the map parameters
        val params = SamplingParams().apply {
            // Another coord systems:
            // https://mousebird.github.io/WhirlyGlobe/reference/android_3_0/com/mousebird/maply/CoordSystem.html
            coordSystem = SphericalMercatorCoordSystem()
            //coverPoles = true
            edgeMatching = true
            minZoom = tileInfo.minZoom
            maxZoom = tileInfo.maxZoom
            singleLevel = true
            setForceMinLevel(true) // ensuring of loading min level tiles
            // setLevelLoads(arrayOf(1, 4, 8, 10, 12).toIntArray())
            // singleLevel = true
            // setClipBounds(Mbr(p1, p2))
        }

        if (customizingType == MapCustomizingType.OLD_LIKE_MAP_TILE_PROVIDER) {
            rotateGestureEnabled = true
        }

        // Set up an image loader, tying all the previous together.
        val loader = QuadImageLoader(params, tileInfo, baseControl)
        loader.setImageFormat(RenderController.ImageFormat.MaplyImageUShort565)
    }

    // The user tapped somewhere, but not on a selectable object.
    override fun userDidTap(mapControl: MapController?, loc: Point2d?, screenLoc: Point2d?) {
        super.userDidTap(mapControl, loc, screenLoc)
        onTapListener?.onTouch(loc, screenLoc)
    }

    override fun userDidTap(globeControl: GlobeController?, loc: Point2d?, screenLoc: Point2d?) {
        super.userDidTap(globeControl, loc, screenLoc)
        onTapListener?.onTouch(loc, screenLoc)
    }

    // The user selected the given object.
    override fun userDidSelect(
        mapControl: MapController?,
        selObjs: Array<out SelectedObject>?,
        loc: Point2d?,
        screenLoc: Point2d?
    ) {
        super.userDidSelect(mapControl, selObjs, loc, screenLoc)
        if (onSelectListener?.onSelect(selObjs, loc, screenLoc) != true) {
            screenLoc?.let { showInfoWindowOnUserSelect(it) }
        }
    }

    private fun showInfoWindowOnUserSelect(screenLoc: Point2d) {
        /*infoWindowCreator?.let {
            val infoWindowView = it.createWindowView()
            val params = ViewGroup.LayoutParams(
                infoWindowView.width,
                infoWindowView.height
            ).apply {

            }
            view.add
        }*/
    }

    override fun userDidSelect(
        globeControl: GlobeController?,
        selObjs: Array<out SelectedObject>?,
        loc: Point2d?,
        screenLoc: Point2d?
    ) {
        super.userDidSelect(globeControl, selObjs, loc, screenLoc)
        onSelectListener?.onSelect(selObjs, loc, screenLoc)
    }

    // Only for MAP type of map, override another for GLOBE
    // The user long pressed somewhere, either on a selectable object or nor
    override fun userDidLongPress(
        mapController: MapController?,
        selObjs: Array<out SelectedObject>?,
        loc: Point2d?,
        screenLoc: Point2d?
    ) {
        super.userDidLongPress(mapController, selObjs, loc, screenLoc)
        onLongPressListener?.onLongPress(selObjs, loc, screenLoc)
    }

    // Go to a specific location with animation
    fun animateToCoords(lat: Double, lon: Double, zoom: Double? = null) {
        // `globeControl` is the controller when using MapDisplayType.Globe
        // `mapControl` is the controller when using MapDisplayType.Map
        // `baseControl` refers to whichever of them is used.

        val p = Point2d.FromDegrees(lon, lat)
        animateToCoords(p, zoom)
    }

    // Go to a specific location with animation
    fun animateToCoords(point: Point2d, zoom: Double? = null) {
        //val latitude = lat * Math.PI / 180  // if put it manually (not via Point2d object)
        //val longitude = lon * Math.PI / 180 // convert it TO RADIANS, NOT use it in DEGREES

        // mapControl.currentMapZoom(point) gives strange values for some reason
        val z = zoom ?: mapControl.currentMapZoom(point)
        mapControl.animatePositionGeo(point.x, point.y, z, ANIMATE_TO_POINT_DURATION_SEC)
    }

    fun addMarker(
        @DrawableRes iconResource: Int, @ColorRes tint: Int,
        lat: Double, lng: Double,
        tag: Any? = null,
        areClickable: Boolean = true
    ): ScreenMarker {
        val markerInfo = MarkerInfo()
        val icon = getBitmap(iconResource, tint)!!
        val markerSize = Point2d(icon.width.toDouble(), icon.height.toDouble())

        val target = ScreenMarker().apply {
            loc = Point2d.FromDegrees(lng, lat) // Longitude, Latitude
            image = icon
            size = markerSize
            color = tint // TODO not works ????
            selectable = areClickable
            userObject = tag
        }

        val markerComponentObject = mapControl.addScreenMarker(
            target,
            markerInfo,
            RenderControllerInterface.ThreadMode.ThreadAny
        )
        componentObjectsForMarkersOnMap[target] = markerComponentObject
        return target
    }

    fun addMarker(
        marker: ScreenMarker,
        markerInfo: MarkerInfo = MarkerInfo(),
        threadMode: RenderControllerInterface.ThreadMode = RenderControllerInterface.ThreadMode.ThreadAny
    ): ScreenMarker {
        val markerComponentObject = mapControl.addScreenMarker(
            marker,
            markerInfo,
            threadMode
        )
        componentObjectsForMarkersOnMap[marker] = markerComponentObject
        return marker
    }

    fun addVector(
        vector: VectorObject, @ColorRes tint: Int,
        lineWidth: Float = DEFAULT_VECTOR_LINE_WIDTH,
        priority: Int = Int.MAX_VALUE,
        areClickable: Boolean = true
    ): VectorObject = addVector(
        vector.apply {
            selectable = areClickable
        },
        VectorInfo().apply {
            setColor(tint)
            setLineWidth(lineWidth)
            drawPriority = priority
        })

    fun addVector(vector: VectorObject, vectorInfo: VectorInfo): VectorObject {
        val markerComponentObject = mapControl.addVector(
            vector,
            vectorInfo,
            RenderControllerInterface.ThreadMode.ThreadAny
        )
        componentObjectsForVectorsOnMap[vector] = markerComponentObject
        return vector
    }

    fun removeMarker(marker: ScreenMarker) {
        componentObjectsForMarkersOnMap[marker]?.let {
            baseControl.removeObject(it, RenderControllerInterface.ThreadMode.ThreadCurrent)
            componentObjectsForMarkersOnMap.remove(marker)
        }
    }

    fun removeVector(vector: VectorObject) {
        componentObjectsForVectorsOnMap[vector]?.let {
            baseControl.removeObject(it, RenderControllerInterface.ThreadMode.ThreadAny)
            componentObjectsForVectorsOnMap.remove(vector)
        }
    }

    fun interface OnWhirlyGlobeControlStartedListener {
        fun onControlStarted()
    }

    fun interface OnWhirlyGlobeTappedListener {
        fun onTouch(loc: Point2d?, screenLoc: Point2d?)
    }

    fun interface OnWhirlyGlobeLongPressListener {
        fun onLongPress(
            selObjs: Array<out SelectedObject>?,
            loc: Point2d?,
            screenLoc: Point2d?
        )
    }

    fun interface OnWhirlyGlobeSelectObjectListener {
        // true if we are completed to handle the action
        fun onSelect(
            selObjs: Array<out SelectedObject>?,
            loc: Point2d?,
            screenLoc: Point2d?
        ): Boolean
    }

    enum class MapCustomizingType(
        val tileProviderUrl: String,
        val cacheDirName: String,
        val headers: Map<String, String> = mapOf()
    ) {
        BLACK_WHITE_JUST_BASE_MAP(
            BLACK_WHITE_MAP_TILE_PROVIDER_URL,
            BLACK_WHITE_CACHE_DIR_NAME,
        ),
        OLD_LIKE_MAP_TILE_PROVIDER(
            OLD_LIKE_MAP_TILE_PROVIDER_URL,
            OLD_LIKE_MAP_CACHE_DIR_NAME
        ),
        THUNDERFOREST_MAP_TILE_PROVIDER(
            THUNDERFOREST_TILE_PROVIDER_URL,
            THUNDERFOREST_CACHE_DIR_NAME
        ),
        OSM_PROVIDER(
            OSM_PROVIDER_URL,
            OSM_CACHE_DIR_NAME,
            mapOf("User-Agent" to "${BuildConfig.APPLICATION_ID} ${BuildConfig.VERSION_NAME}")
        ),
        TEST_PROVIDER(
            TEST_TILE_PROVIDER_URL,
            TEST_CACHE_DIR_NAME,
            mapOf()
        )
    }

    companion object {
        var DEBUG_TAG = MyWhirlyGlobeMapFragment::class.java.name

        const val MIN_MAP_ZOOM = 1
        const val MAX_MAP_ZOOM = 12
        const val ANIMATE_TO_POINT_DURATION_SEC = 1.0

        const val DEFAULT_VECTOR_LINE_WIDTH = 10f

        //  https://wiki.openstreetmap.org/wiki/Tiles

        private const val TEST_TILE_PROVIDER_URL = ""
        private const val TEST_CACHE_DIR_NAME = "test"

        private const val OSM_PROVIDER_URL = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
        private const val OSM_CACHE_DIR_NAME = "OSM"

        // like an old map tile provider
        private const val OLD_LIKE_MAP_TILE_PROVIDER_URL ="http://tile.stamen.com/watercolor/{z}/{x}/{y}.png"
        private const val OLD_LIKE_MAP_CACHE_DIR_NAME = "old_style_osm_maps"

        private const val THUNDERFOREST_TILE_PROVIDER_URL = "https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=d459904d13a0435fad08a5a45a47ed0a"
        private const val THUNDERFOREST_CACHE_DIR_NAME = "thunderforest"

        // black-white tile provider
        private const val BLACK_WHITE_MAP_TILE_PROVIDER_URL = "http://tile.stamen.com/toner/{z}/{x}/{y}.png"
        private const val BLACK_WHITE_CACHE_DIR_NAME = "bw_osm_maps"
    }
}