package com.example.osmvsgmapsexample.presentation.osmlibs

import android.annotation.SuppressLint
import android.content.Context
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.databinding.FragmentOsmLibsMapsBinding
import com.example.osmvsgmapsexample.pojo.CarsharingCar
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.pojo.RouteTurnsInstruction
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment
import com.example.osmvsgmapsexample.presentation.gmaps.GoogleMapsExampleFragment
import com.example.osmvsgmapsexample.presentation.main.*
import com.example.osmvsgmapsexample.presentation.osmlibs.MyWhirlyGlobeMapFragment.*
import com.example.osmvsgmapsexample.util.getLastKnownLocationWithTriggering
import com.example.osmvsgmapsexample.util.hideSoftKeyboard
import com.google.android.gms.maps.model.Dash
import com.google.android.gms.maps.model.Gap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PatternItem
import com.google.maps.android.SphericalUtil
import com.mousebird.maply.RenderControllerInterface
import com.mousebird.maply.ScreenMarker
import com.mousebird.maply.VectorInfo
import com.mousebird.maply.VectorObject
import java.util.*

class OsmLibsExampleFragment : BaseMapExampleFragment(), OnWhirlyGlobeControlStartedListener {

    // Try https://wiki.openstreetmap.org/wiki/RU:Nominatim to perform geocoding operations ?
    // Try https://wiki.openstreetmap.org/wiki/RU:Overpass_API to search places ????

    lateinit var binding: FragmentOsmLibsMapsBinding

    private var carMarkers = mutableListOf<ScreenMarker>()

    private var currentCarMarker: ScreenMarker? = null
    private var userLocationMarker: ScreenMarker? = null

    private var currentDirectGeocodingResultMarker: ScreenMarker? = null
    private var currentReverseGeocodingResultMarker: ScreenMarker? = null

    private var routeVector: VectorObject? = null

    // Used to work with the map
    lateinit var mapFragment: MyWhirlyGlobeMapFragment

    // Location manager are used to observe user's location
    private var locationManager: LocationManager? = null

    // Last known location fetched by locationManager
    // would be null if no access has been granted
    private val lastKnownLocation: Location?
        @SuppressLint("MissingPermission") get() = locationManager?.let {
            getLastKnownLocation(it)
        }

    private val stateLeaveCar = R.string.leave_car_button_text to {
        leaveCar()
    }
    private val stateSitInCar = R.string.sit_in_car_button_text to {
        // sit in car if we're close enough
        viewModel.closestCar.value?.let { sitInCar(it) }
        Unit
    }

    override val DEFAULT_WALKING_ZOOM_LEVEL = 0.0001f
    override val DEFAULT_DRIVING_ZOOM_LEVEL = 0.0005f
    override val DEFAULT_SEARCH_RESULT_ZOOM = 0.0003f

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_osm_libs_maps, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapFragment =
            childFragmentManager.findFragmentById(R.id.map_view) as MyWhirlyGlobeMapFragment
        mapFragment.onControlStartedListener = this
        initPrimaryButtonState()
        initSearchingFeature()
        binding.viewModel = viewModel as OsmLibsExampleViewModel
        binding.myLocationButton.setOnClickListener {
            val z = if (viewModel.userInCar.value == null) DEFAULT_WALKING_ZOOM_LEVEL else DEFAULT_DRIVING_ZOOM_LEVEL
            getUserMapLocation()?.toPoint2d()?.let { loc -> mapFragment.animateToCoords(loc, z.toDouble()) }
        }
    }

    private fun initPrimaryButtonState() {
        val osmViewModel = viewModel as OsmLibsExampleViewModel
        if (osmViewModel.bottomButtonStatus.get() == null)
            osmViewModel.bottomButtonStatus.set(stateSitInCar)

        viewModel.userInCar.observe(viewLifecycleOwner) {
            osmViewModel.bottomButtonStatus.set(if (it == null) stateSitInCar else stateLeaveCar)
        }

        binding.primaryButton.setOnClickListener {
            osmViewModel.bottomButtonStatus.get()?.second?.invoke()
        }
    }

    private fun initSearchingFeature() {
        binding.clearSearchButton.setOnClickListener {
            hideSoftKeyboard(requireActivity())
            viewModel.searchingLocationName.set("")
            viewModel.directGeocodingResult.postValue(null)
        }
        binding.searchEdit.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                binding.isSearching = true
                hideSoftKeyboard(requireActivity())
                viewModel.requestDirectGeocodingCurrentRequestSearch()
            }
            true
        }
        binding.searchResultsRecycler.adapter = searchingResultsListAdapter
        binding.searchResultsRecycler.itemAnimator = null

        // direct geocoding
        (viewModel as OsmLibsExampleViewModel).directGeocodingKostilForSearching.observe(
            viewLifecycleOwner
        ) {
            binding.isSearching = false
            val result = Geocoder(requireContext(), Locale.getDefault()).getFromLocationName(
                it,
                GoogleMapsExampleFragment.MAX_GEOCODING_RESULTS
            )
            viewModel.directGeocodingResult.postValue(result.map {
                GeocodingResult(
                    it.getAddressLine(
                        it.maxAddressLineIndex
                    ), it.latitude, it.longitude
                )
            })
        }

        // reverse geocoding
        (viewModel as OsmLibsExampleViewModel).reverseGeocodingKostilForSearching.observe(
            viewLifecycleOwner
        ) {
            val result =
                Geocoder(requireContext(), Locale.getDefault()).getFromLocation(it.lat, it.lng, 1)
            val oneResult = result[0]
            viewModel.reverseGeocodingResult.postValue(
                GeocodingResult(
                    oneResult.getAddressLine(
                        oneResult.maxAddressLineIndex
                    ), oneResult.latitude, oneResult.longitude
                )
            )
        }

    }

    override fun customizeMap() {

    }

    override fun onReverseGeocodingResult(result: GeocodingResult?) {
        result?.let { currentReverseGeocodingResultMarker = addGeocodingResultMarker(it) }
    }

    // == Gmaps OnMapReady
    override fun onControlStarted() {
        customizeMap()

        mapFragment.onTapListener = OnWhirlyGlobeTappedListener { loc, screenLoc ->
            loc?.let { onMapClick(it.toMapLocationWithToDegreesConvert()) }
        }
        mapFragment.onLongPressListener =
            OnWhirlyGlobeLongPressListener { selObjs, loc, screenLoc ->
                loc?.let { requestReverseGeocodingOnLongClick(it.toMapLocationWithToDegreesConvert()) }
            }
        mapFragment.onSelectListener =
            OnWhirlyGlobeSelectObjectListener { selObjs, loc, screenLoc ->
                val value = selObjs?.getNearest()

                value?.selObj?.let {
                    if (it is ScreenMarker) { // or is VectorObject
                        when (it.userObject) {
                            is CarsharingCar -> {
                                onCarClick(it.userObject as CarsharingCar)
                                viewModel.userInCar.value != null
                            }
                            is GeocodingResult -> {
                                val result = it.userObject as GeocodingResult
                                mapFragment.animateToCoords(result.toPoint2d())
                                false
                            }
                            else -> true
                        }
                    } else true
                } ?: true
            }

        viewModel.areUserGrantedLocation.observe(viewLifecycleOwner) {
            if (it) {
                afterPermsResolved()
            }
        }
        viewModel.loadAvailableCars()
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this).get(OsmLibsExampleViewModel::class.java)
        (viewModel as OsmLibsExampleViewModel).googleApiKey = getString(R.string.gmaps_api_key)
    }

    @SuppressLint("MissingPermission")
    override fun afterPermsResolved() {
        // map.isMyLocationEnabled = true
        activity?.let {
            locationManager = it.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        }
    }

    override fun drawAvailableCars(values: List<CarsharingCar>) {
        carMarkers.forEach { mapFragment.removeMarker(it) }
        carMarkers = values.map { addCarMarker(it) }.toMutableList()
    }

    private fun addCarMarker(car: CarsharingCar) = mapFragment.addMarker(
        R.drawable.ic_car,
        android.R.color.black,
        car.lat,
        car.lng,
        car,
        areClickable = true
    )

    private fun addUserWalkLocationMarker(location: MapLocation) = mapFragment.addMarker(
        R.drawable.ic_baseline_directions_walk_24,
        android.R.color.black,
        location.lat,
        location.lng,
        areClickable = false
    )

    private fun addGeocodingResultMarker(result: GeocodingResult) = mapFragment.addMarker(
        R.drawable.ic_baseline_location,
        android.R.color.holo_red_dark,
        result.lat,
        result.lng,
        result,
        areClickable = true
    )

    override fun removeUserCarMarkerFromMap() {
        currentCarMarker?.let { mapFragment.removeMarker(it) }
    }

    override fun placeUserCarMarkerOnMap(car: CarsharingCar) {
        currentCarMarker = addCarMarker(car)
    }

    @SuppressLint("MissingPermission")
    override fun removeUserLocationMarkerFromMap() {
        userLocationMarker?.let {
            mapFragment.removeMarker(it)
        }
    }

    @SuppressLint("MissingPermission")
    override fun placeOrUpdateUserLocationMarkerOnMap() {
        getUserMapLocation()?.let {
            removeUserLocationMarkerFromMap()
            userLocationMarker = addUserWalkLocationMarker(it)
        }
    }

    override fun onSearchResultClick(result: GeocodingResult) {
        currentDirectGeocodingResultMarker?.let {
            mapFragment.removeMarker(it)
        }

        currentDirectGeocodingResultMarker = addGeocodingResultMarker(result)
        mapFragment.animateToCoords(
            result.lat,
            result.lng,
            DEFAULT_SEARCH_RESULT_ZOOM.toDouble()
        )
    }

    override fun moveCurrentCarMarkerToLocation(location: MapLocation, withDegrees: Double?) {
        currentCarMarker?.let { marker ->
            marker.loc = location.toPoint2d()
            withDegrees?.let {
                marker.rotation = it - 180 // => minus drawable rotation itself
            }
            removeUserCarMarkerFromMap()
            mapFragment.addMarker(
                marker,
                threadMode = RenderControllerInterface.ThreadMode.ThreadCurrent
            )
        }
    }

    override fun zoomOnUserLocation(zoomLevel: Float) {
        val userLocation = getUserMapLocation()?.toPoint2d()
        userLocation?.let {
            mapFragment.animateToCoords(it, DEFAULT_WALKING_ZOOM_LEVEL.toDouble())
        }
    }

    override fun onDestinationPointChosen(mapLocation: MapLocation) {
        val startLocation = getUserMapLocation()
        startLocation?.let {
            val movementType =
                if (viewModel.userInCar.value == null) MapMovementType.WALKING else MapMovementType.DRIVING
            viewModel.requestRoute(it, mapLocation, movementType = movementType)
        }
    }

    override fun drawRouteInstructions(instructions: List<RouteTurnsInstruction>) {

    }

    override fun drawRoutePolyline(line: List<MapLocation>, inErrorState: Boolean?) {
        routeVector?.let {
            mapFragment.removeVector(it)
        }
        // viewModel.userInCar.notifyChange()
        routeVector = VectorObject().apply {
            this.addLinear(line.map { it.toPoint2d() }.toTypedArray())
        }
        val info = getAccordingVectorInfo(inErrorState) // TODO not works ????
        mapFragment.addVector(routeVector!!, info)
    }

    private fun getAccordingVectorInfo(areErrorOccured: Boolean?) = when (areErrorOccured) {
        true -> getErrorStyledVectorInfo()
        false -> getNormalStyledVectorInfo()
        null -> getBadRouteStylePolyline()
    }

    private fun getNormalStyledVectorInfo() = VectorInfo().apply {
        setLineWidth(DEFAULT_LINE_WIDTH_PX)
        setColor(ResourcesCompat.getColor(resources, R.color.color_map_route_ordinary, context?.theme))
        drawPriority = Int.MAX_VALUE
    }

    private fun getErrorStyledVectorInfo() = VectorInfo().apply {
        setLineWidth(DEFAULT_LINE_WIDTH_PX)
        setColor(ResourcesCompat.getColor(resources, R.color.color_map_bad_route, context?.theme))
        drawPriority = Int.MAX_VALUE
    }

    private fun getBadRouteStylePolyline() = VectorInfo().apply {
        setLineWidth(DEFAULT_LINE_WIDTH_PX)
        setColor(ResourcesCompat.getColor(resources, R.color.color_map_bad_route, context?.theme))
        drawPriority = Int.MAX_VALUE
    }

    override fun onCancelRouteBuild() {
        routeVector?.let {
            mapFragment.removeVector(it)
            viewModel.availableCars.notifyChange()
        }
    }

    // Use google maps classes (I had not found ready-to-use way to calculate distances with this lib)
    override fun getDistanceBetweenInMeters(l1: MapLocation, l2: MapLocation): Double =
        getDistanceBetweenInMeters(l1.toGoogleMapsLatLng(), l2.toGoogleMapsLatLng())

    private fun getDistanceBetweenInMeters(l1: LatLng, l2: LatLng): Double =
        SphericalUtil.computeDistanceBetween(l1, l2)

    override fun getUserMapLocation(): MapLocation? =
        lastKnownLocation?.toMapLocationWithToDegreesConvert()

    override fun setSitInCarActionAvailable(isAvailable: Boolean) {
        binding.primaryButton.visibility = if (isAvailable) View.VISIBLE else View.GONE
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(locationManager: LocationManager): Location? {
        val providers = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val currentProviderLastLocation: Location =
                locationManager.getLastKnownLocationWithTriggering(provider)
                    ?: continue
            if (bestLocation == null || currentProviderLastLocation.accuracy < bestLocation.accuracy) {
                bestLocation = currentProviderLastLocation
            }
        }
        return bestLocation
    }

    companion object {
        private const val DEFAULT_LINE_WIDTH_PX = 7f
        private const val PATTERN_GAP_DASH_LENGTH_PX = 20f
        private val DASH: PatternItem = Dash(PATTERN_GAP_DASH_LENGTH_PX)
        private val GAP: PatternItem = Gap(PATTERN_GAP_DASH_LENGTH_PX)

        private val POLYGON_LIST_STROKE = listOf(GAP, DASH)
        const val MAX_GEOCODING_RESULTS = 3
    }
}