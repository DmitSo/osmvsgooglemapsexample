package com.example.osmvsgmapsexample.presentation.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.osmvsgmapsexample.R
import com.example.osmvsgmapsexample.databinding.ItemSearchResultsListBinding
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.presentation.base.BaseDirectGeocodingSearchResultsListAdapter.GeocodingResultViewHolder

class BaseDirectGeocodingSearchResultsListAdapter :
    ListAdapter<GeocodingResult, GeocodingResultViewHolder>(
        object : DiffUtil.ItemCallback<GeocodingResult>() {
            override fun areItemsTheSame(
                oldItem: GeocodingResult,
                newItem: GeocodingResult
            ): Boolean = oldItem === newItem

            override fun areContentsTheSame(oldItem: GeocodingResult, newItem: GeocodingResult) =
                oldItem == newItem
        }
    ) {

    var onResultClick: (item: GeocodingResult) -> Unit = {}

    var items: List<GeocodingResult> = listOf()
        set(value) {
            field = value
            submitList(value)
        }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeocodingResultViewHolder {
        val binding = DataBindingUtil.inflate<ItemSearchResultsListBinding>(
            LayoutInflater.from(parent.context), R.layout.item_search_results_list, parent, false
        )
        return GeocodingResultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GeocodingResultViewHolder, position: Int) {
        val item = items[position]
        holder.binding.result = item
        holder.binding.root.setOnClickListener { onResultClick(item) }
    }

    inner class GeocodingResultViewHolder(val binding: ItemSearchResultsListBinding) :
        RecyclerView.ViewHolder(binding.root)
}