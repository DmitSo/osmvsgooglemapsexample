package com.example.osmvsgmapsexample.util

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity


fun hideSoftKeyboard(activity: Activity) {
    activity.currentFocus?.let { focus ->
        val inputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(focus.windowToken, 0)
    }
}

fun hideSoftKeyboard(activity: FragmentActivity) {
    activity.currentFocus?.let { focus ->
        val inputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(focus.windowToken, 0)
    }
}

fun Fragment.getBitmap(drawableRes: Int, tintColor: Int? = null): Bitmap? = context?.let { ctx ->
    AppCompatResources.getDrawable(ctx, drawableRes)?.let { unwrappedDrawable ->
        val drawable = DrawableCompat.wrap(unwrappedDrawable)

        // Set tint
        tintColor?.let { tintClr ->
            val color = ResourcesCompat.getColor(resources, tintClr, ctx.theme)
            drawable.mutate().colorFilter =
                BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                    color, BlendModeCompat.SRC_IN
                )
        }

        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)
        bitmap
    }
}

// required because without it appears bug when getting the same location each time we call
// locationManager.getLastKnownLocation
@SuppressLint("MissingPermission")
fun LocationManager.getLastKnownLocationWithTriggering(provider: String): Location? {
    requestLocationUpdates(provider, 0, .0f, AppUtilCompanion.locationListener)
    return getLastKnownLocation(provider)
}

object AppUtilCompanion {
    val locationListener = object : LocationListener {
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle?) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
        override fun onLocationChanged(location: Location) {}
    }
}