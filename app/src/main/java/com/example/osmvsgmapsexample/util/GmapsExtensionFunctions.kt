package com.example.osmvsgmapsexample.util

import com.example.osmvsgmapsexample.domain.repository.RoutesApiRequestLocation
import com.example.osmvsgmapsexample.domain.repository.RoutesApiRequestMovementType
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment

fun BaseMapExampleFragment.MapMovementType.toGoogleApiRequestMovementType() = when(this) {
    BaseMapExampleFragment.MapMovementType.WALKING -> RoutesApiRequestMovementType.WALKING
    BaseMapExampleFragment.MapMovementType.DRIVING -> RoutesApiRequestMovementType.DRIVING
}

fun BaseMapExampleFragment.MapLocation.toGoogleApiRequestLocation() = RoutesApiRequestLocation(lat, lng)
fun RoutesApiRequestLocation.toMapLocation() = BaseMapExampleFragment.MapLocation(lat, lng)
