package com.example.osmvsgmapsexample.presentation.main

import android.R
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory


fun bitmapDescriptorFromVector(
    context: Context,
    @DrawableRes vectorResId: Int,
    @ColorRes tint: Int? = null
): BitmapDescriptor? {
    val vectorDrawable: Drawable = ContextCompat.getDrawable(context, vectorResId)!!
    vectorDrawable.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
    if (tint != null) vectorDrawable.setTint(context.resources.getColor(tint))
    val bitmap = Bitmap.createBitmap(
        vectorDrawable.intrinsicWidth,
        vectorDrawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    vectorDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

fun bitmapDescriptorFromVectorColored(
    context: Context,
    @DrawableRes vectorResId: Int,
    tint: Int = Color.BLACK
): BitmapDescriptor? {
    val unwrappedDrawable = AppCompatResources.getDrawable(context, vectorResId)
    val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable!!)
    DrawableCompat.setTint(wrappedDrawable, tint)

    val bitmap = Bitmap.createBitmap(
        wrappedDrawable.intrinsicWidth,
        wrappedDrawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    wrappedDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

@BindingAdapter("areVisibleElseGone")
fun setVisibilityOrGone(view: View, areVisible: Boolean) {
    view.isVisible = areVisible
}