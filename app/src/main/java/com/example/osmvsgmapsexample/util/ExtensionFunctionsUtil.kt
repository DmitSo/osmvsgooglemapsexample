package com.example.osmvsgmapsexample.presentation.main

import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.example.osmvsgmapsexample.pojo.CarsharingCar
import com.example.osmvsgmapsexample.pojo.GeocodingResult
import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment.MapLocation
import com.google.android.gms.maps.model.LatLng
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.geojson.Point
import com.mousebird.maply.Point2d
import com.mousebird.maply.SelectedObject

fun <T> MutableLiveData<T>.notifyChange() {
    value = value
}

// App pojo objects
fun CarsharingCar.getMapLocation() = MapLocation(lat, lng)

// Google maps
fun MapLocation.toGoogleMapsLatLng() = LatLng(lat, lng)
fun Location.toMapLocationWithToDegreesConvert() = MapLocation(latitude, longitude)
fun LatLng.toMapLocationWithToDegreesConvert() = MapLocation(latitude, longitude)

// WhirlyGlobe
fun MapLocation.toPoint2d() = Point2d.FromDegrees(lng, lat)
fun Point2d.toMapLocationWithToDegreesConvert() = with(this.toDegrees()) { MapLocation(y, x) }
fun Array<out SelectedObject>.getNearest() = this.minByOrNull { it.screenDist }
fun GeocodingResult.toPoint2d() = Point2d.FromDegrees(lng, lat)

// Mapbox
fun com.mapbox.mapboxsdk.geometry.LatLng.toMapLocation() = MapLocation(latitude, longitude)
fun MapLocation.toMapboxLatLng() = com.mapbox.mapboxsdk.geometry.LatLng(lat, lng)
fun CarmenFeature.toGeocodingResult() = with(this.center()!!) {
    GeocodingResult(placeName() ?: "", latitude(), longitude())
}
fun MapLocation.toPointMapbox() = Point.fromLngLat(lng, lat)
