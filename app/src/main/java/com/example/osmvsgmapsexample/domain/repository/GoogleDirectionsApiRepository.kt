package com.example.osmvsgmapsexample.domain.repository

import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.maps.DirectionsApi
import com.google.maps.GeoApiContext
import com.google.maps.errors.NotFoundException
import com.google.maps.model.DirectionsResult
import com.google.maps.model.LatLng
import com.google.maps.model.TravelMode
import java.util.concurrent.TimeUnit

class GoogleDirectionsApiRepository : RoutesApiRepository {

    var apiKey: String? = null
        set(value) {
            geoApiContext = GeoApiContext.Builder()
                .apiKey(value)
                .retryTimeout(60, TimeUnit.SECONDS)
                .maxRetries(3)
                .build()
            field = value
        }

    var geoApiContext: GeoApiContext? = null
        get() {
            return field ?: throw IllegalStateException(
                "GeoApiContext hasn't been initialized. " +
                        "Initialize it by settings apiKey in ${this.javaClass.name}"
            )
        }

    override fun requestRouteFromApi(
        movementType: RoutesApiRequestMovementType,
        vararg value: RoutesApiRequestLocation
    ): LiveData<RoutesApiRequestRouteResult> {
        require(value.size >= 2) { "not enough path points for building route" }

        val task = DownloadTask(geoApiContext!!)
        val result = task.resultLiveData
        val apiRequestResult = RoutesApiRouteRequest().apply {
            startLocation = value.first()
            endLocation = value.last()
            this.movementType = movementType
            this.route = value.toMutableList()
        }
        task.execute(apiRequestResult)
        return result
    }

    private class DownloadTask(val geoApiContext: GeoApiContext) :
        AsyncTask<RoutesApiRouteRequest, Void, DirectionsResult>() {

        private var requestValue: RoutesApiRouteRequest? = null
        private val resultMutableLiveData = MutableLiveData<RoutesApiRequestRouteResult>()
        val resultLiveData: LiveData<RoutesApiRequestRouteResult> = resultMutableLiveData

        override fun doInBackground(vararg params: RoutesApiRouteRequest?): DirectionsResult? {
            requestValue = params[0]
            return with(requestValue!!) {
                try {
                    DirectionsApi.newRequest(geoApiContext)
                        .mode(movementType!!.toGoogleApiTravelMode())
                        .origin(startLocation!!.toGoogleApiLatLng())
                        .destination(endLocation!!.toGoogleApiLatLng())
                        .also { req ->
                            if (route?.size ?: 0 > 2) req.waypoints(*route!!.map { it.toGoogleApiLatLng() }
                                .toTypedArray())
                        }
                        .await()
                } catch (nfe: NotFoundException) {
                    Log.w(DEBUG_TAG, nfe.localizedMessage ?: nfe.message ?: "", nfe)
                    null
                }
            }
        }

        override fun onPostExecute(apiResult: DirectionsResult?) {
            super.onPostExecute(apiResult)
            val resultValue = RoutesApiRequestRouteResult()
            resultValue.areErrorOccured = apiResult?.routes?.isEmpty() != false

            val route =
                mutableListOf<RoutesApiRequestLocation>().apply { add(requestValue!!.startLocation!!) }
            if (resultValue.areErrorOccured == false) {
                val resultPath = apiResult!!.routes
                    .map { it.overviewPolyline.decodePath() }
                    .flatten()
                    .map { it.toRoutesApiLocation() }
                route.addAll(resultPath)

                val leg = apiResult.routes[0].legs[0]
                resultValue.durationMinutes = (leg.duration.inSeconds / 60).toInt()
                resultValue.distanceMeters = leg.distance.inMeters.toInt()
            }
            route.add(requestValue!!.endLocation!!)
            resultValue.route = route

            resultMutableLiveData.postValue(resultValue)
        }
    }

    companion object {
        val DEBUG_TAG = GoogleDirectionsApiRepository::class.java.name
    }
}

private fun RoutesApiRequestMovementType.toGoogleApiTravelMode() = when (this) {
    RoutesApiRequestMovementType.WALKING -> TravelMode.WALKING
    RoutesApiRequestMovementType.DRIVING -> TravelMode.DRIVING
}

private fun RoutesApiRequestLocation.toGoogleApiLatLng() = LatLng(lat, lng)

private fun LatLng.toRoutesApiLocation() = RoutesApiRequestLocation(lat, lng)