package com.example.osmvsgmapsexample.domain.repository

import androidx.lifecycle.LiveData

interface RoutesApiRepository {
    fun requestRouteFromApi(movementType: RoutesApiRequestMovementType, vararg value: RoutesApiRequestLocation): LiveData<RoutesApiRequestRouteResult>
}

enum class RoutesApiRequestMovementType {
    WALKING,
    DRIVING
}

data class RoutesApiRequestLocation(val lat: Double, val lng: Double)

class RoutesApiRequestRouteResult {
    var startLocation: RoutesApiRequestLocation? = null
    var endLocation: RoutesApiRequestLocation? = null
    var route: List<RoutesApiRequestLocation>? = null
    var movementType: RoutesApiRequestMovementType? = null
    var areErrorOccured: Boolean? = null
    var durationMinutes: Int = 0
    var distanceMeters: Int = 0
}

class RoutesApiRouteRequest {
    var startLocation: RoutesApiRequestLocation? = null
    var endLocation: RoutesApiRequestLocation? = null
    var route: List<RoutesApiRequestLocation>? = null
    var movementType: RoutesApiRequestMovementType? = null
}