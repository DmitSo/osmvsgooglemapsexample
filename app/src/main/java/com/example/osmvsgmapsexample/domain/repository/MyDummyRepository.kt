package com.example.osmvsgmapsexample.domain.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.osmvsgmapsexample.pojo.CarsharingCar

class MyDummyRepository {

    // Let's just close our eyes and imagine that it makes a call to some our
    // backend service with retrofit
    val myValues = listOf(
        CarsharingCar(1, "Lada Granta", "9668 IB-3", "#ffffff", "white", 52.4182, 30.9743),
        CarsharingCar(2, "BMW X5", "9999 II-3", "#000000", "black", 52.4189, 30.9744),
        CarsharingCar(3, "Honda CR-V", "1122 BI-3", "#000000", "black", 52.4132, 30.9742),
        CarsharingCar(4, "Honda RX-5", "1111 BI-3", "#000000", "black", 52.4185, 30.9744)
    )

    fun getAvailableCars(): LiveData<List<CarsharingCar>> {
        return MutableLiveData<List<CarsharingCar>>().apply { postValue(myValues) }
    }

    fun occupyCar(carId: Long, userId: Long): LiveData<Boolean> {
        return MutableLiveData<Boolean>().apply {
            val car = myValues.firstOrNull { it.id == carId }
            car?.let {
                it.occupiedByUserId = userId
                postValue(true)
            } ?: postValue(false)
        }
    }

    fun leaveCar(userId: Long, carLat: Double, carLng: Double): LiveData<Boolean> {
        return MutableLiveData<Boolean>().apply {
            val car = myValues.firstOrNull { it.occupiedByUserId == userId }
            car?.let {
                it.occupiedByUserId = null
                it.lat = carLat
                it.lng = carLng
                postValue(true)
            } ?: postValue(false)
        }
    }
}