package com.example.osmvsgmapsexample.pojo

import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment

data class CarsharingCar(
    val id: Long,
    val markkaAndModelString: String,
    val vehicleNumber: String,
    val colorHexString: String,
    val colorName: String,
    var lat: Double,
    var lng: Double,
    var occupiedByUserId: Long? = null
)

fun CarsharingCar.getMapLocation() = BaseMapExampleFragment.MapLocation(lat, lng)