package com.example.osmvsgmapsexample.pojo

import com.example.osmvsgmapsexample.presentation.base.BaseMapExampleFragment

data class GeocodingResult(
    val formattedName: String,
    val lat: Double,
    val lng: Double
)

fun GeocodingResult.toMapLocation() = BaseMapExampleFragment.MapLocation(lat, lng)