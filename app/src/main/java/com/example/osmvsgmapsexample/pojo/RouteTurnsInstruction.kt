package com.example.osmvsgmapsexample.pojo

data class RouteTurnsInstruction(
    val distanceMeters: Double,
    val direction: TurnDirection
) {
    enum class TurnDirection {
        LEFT,
        RIGHT,
        FORWARD,
        REVERSE
    }
}